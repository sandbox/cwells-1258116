(function ($) {
  Drupal.behaviors.link_track = {
    attach: function(context) {
      $('a.link_track').each(function () {
        var url = $(this).attr("href");
        $(this).attr('href', Drupal.settings.basePath+'obt?obthref='+encodeURIComponent(url));
      });
    }
  }
})(jQuery);